package aviary;

import animal.Animal;

public abstract class Aviary {
    public abstract void addAnimal (Animal animal);

    public abstract void delete (int index);

    public abstract Animal getAnimal (int index);

}
