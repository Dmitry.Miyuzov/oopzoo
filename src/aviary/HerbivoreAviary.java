package aviary;

import animal.Animal;
import animal.Herbivore;

public class HerbivoreAviary extends Aviary {

    private Animal[] aviary;

    public HerbivoreAviary(int sizeAviary) {
        if (sizeAviary < 1) {
            aviary = new Herbivore[1];
            System.out.println("Данный размер не подходит. Был создан вольер с размером 1.");
        } else {
            aviary = new Herbivore[sizeAviary];
        }
    }

    @Override
    public void addAnimal(Animal animal) {
        boolean isAdded = false;
        if (animal instanceof Herbivore) {
            for (int i = 0; i < aviary.length; i++) {
                if (aviary[i] == null) {
                    aviary[i] = animal;
                    isAdded = true;
                    break;
                }
            }
            if (!isAdded) {
                System.out.println("Вольер заполнен.");
            } else {
                System.out.println(animal.getDescription() + " в вольере.");
            }
        } else {
            System.out.println(animal.getDescription() + " - не может находится в вольере для травоядных.");
        }
    }

    @Override
    public void delete(int index) {
        aviary[index] = null;
    }

    @Override
    public Animal getAnimal(int index) {
        return aviary[index];
    }
}
