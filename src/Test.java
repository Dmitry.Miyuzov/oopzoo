import animal.Animal;
import animal.Chicken;
import animal.Herbivore;
import animal.Wolf;
import aviary.Aviary;
import aviary.CarnivorousAviary;
import aviary.HerbivoreAviary;
import food.Corn;
import food.Food;
import food.Meat;

public class Test {
    public static void main(String[] args) {
        Food meat = new Meat();
        Food corn = new Corn();
        Animal wolf1 = new Wolf();
        Animal wolf2 = new Wolf();
        Animal wolf3 = new Wolf();
        Animal wolf4 = new Wolf();
        Animal chicken1 = new Chicken();
        Animal chicken2 = new Chicken();
        Animal chicken3 = new Chicken();
        Animal chicken4 = new Chicken();
        Aviary aviary = new HerbivoreAviary(3);
        Aviary aviary2 = new CarnivorousAviary(3);
   //     aviary.addAnimal(wolf1); // Пробуем добавить волка в вольер для травоядных
   //       aviary2.addAnimal(chicken1); //Пробуем добавить курицу в вольер для плотоядных.

        /* Для правильно работы расскоментить 3 строки. Для ошибки что вольер заполнен 4 строки.
        aviary.addAnimal(chicken1);
        aviary.addAnimal(chicken2);
        aviary.addAnimal(chicken3);
        aviary.addAnimal(chicken4);
         */

        /* По принципу выше.
        aviary2.addAnimal(wolf1);
        aviary2.addAnimal(wolf2);
        aviary2.addAnimal(wolf3);
        aviary2.addAnimal(wolf4);
        */


    }
}