package animal;

import animal.Animal;
import food.Food;
import food.HerbivoreFood;

public abstract class Carnivorous extends Animal {
    @Override
    public void eat(Food food) {
      if (food instanceof HerbivoreFood) {
          System.out.println(this.getDescription() + " не кушает эту еду.");
      } else {
          System.out.println(this.getDescription() + " - кушает");
      }
    }
}
