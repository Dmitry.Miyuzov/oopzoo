package animal;

import animal.Animal;
import food.CarnivorousFood;
import food.Food;

public abstract class Herbivore extends Animal {
    @Override
    public void eat(Food food) {
        if (food instanceof CarnivorousFood) {
            System.out.println(this.getDescription() + " не кушает эту еду.");
        } else {
            System.out.println(this.getDescription() + " - кушает");
        }
    }
}
