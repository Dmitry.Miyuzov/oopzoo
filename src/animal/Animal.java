package animal;

import food.Food;

public abstract class Animal {
    private String description;
    public abstract void eat(Food food);

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
